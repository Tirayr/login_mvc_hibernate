package am.egs.hibernate.service;

import am.egs.hibernate.model.Student;

public interface StudentService {
    void add(Student student);

    Student getStudent(String email);

}
