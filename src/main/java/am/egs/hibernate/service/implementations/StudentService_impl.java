package am.egs.hibernate.service.implementations;

import am.egs.hibernate.model.Student;
import am.egs.hibernate.repository.StudentRepository;
import am.egs.hibernate.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class StudentService_impl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    @Transactional
    public void add(Student student) {
        studentRepository.add(student);
    }

    @Override
    public Student getStudent(String email) {
        return studentRepository.getByEmail(email);
    }
}
