package am.egs.hibernate.repository.implementations;

import am.egs.hibernate.model.Student;
import am.egs.hibernate.repository.StudentRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class StudentRepository_impl implements StudentRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void add(Student student) {
        sessionFactory.openSession().save(student);
    }

    @Override
    public Student getByEmail(String email) {
      return (Student) sessionFactory.getCurrentSession().get(Student.class,email);
    }
}
