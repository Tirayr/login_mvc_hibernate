package am.egs.hibernate.repository;

import am.egs.hibernate.model.Student;

public interface StudentRepository {
    void add(Student student);

    Student getByEmail(String email);
}
