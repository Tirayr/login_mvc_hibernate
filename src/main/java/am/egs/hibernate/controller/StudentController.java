package am.egs.hibernate.controller;


import am.egs.hibernate.model.Student;
import am.egs.hibernate.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private Student student;

    @RequestMapping(value = "/register",method = RequestMethod.POST)
    public ModelAndView registratio(@RequestParam String name,
                                    @RequestParam String email,
                                    @RequestParam String profession,
                                    @RequestParam String password,
                                    HttpSession session){
        student.setName(name);
        student.setProfession(profession);
        student.setEmail(email);
        student.setPassword(password);

        studentService.add(student);
        session.setAttribute("student",student);
        return new ModelAndView("home");
    }



    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ModelAndView login(HttpSession session,
                              HttpServletRequest request){

        String email = request.getParameter("email");
        String password = request.getParameter("password");
        student = studentService.getStudent(email);
        if (!student.getPassword().equals(password)){
            return new ModelAndView("index","message","Wrong email or password");
        }
        session.setAttribute("student",student);
        return new ModelAndView("home");
    }
}
