package am.egs.hibernate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;


@Controller
public class RenderController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String start(){
        return "index";
    }

    @RequestMapping(value = "/goToRegister", method = RequestMethod.GET)
    public String goToRegister(){
        return "register";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpSession session){
        session.invalidate();
        return "index";
    }

}
